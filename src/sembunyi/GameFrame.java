package sembunyi;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class GameFrame {
    private JFrame f = new JFrame();
    private GamePanel p = new GamePanel();

    public GameFrame() {
        f.setLayout(new BorderLayout());
        f.setSize(500, 500);
        f.setVisible(true);
        f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        f.add(p);

    }

    public static void main(String[] args) {
        new GameFrame();
    }
}
