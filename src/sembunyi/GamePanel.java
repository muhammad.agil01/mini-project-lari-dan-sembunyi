package sembunyi;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.HashSet;
import java.util.Set;

public class GamePanel extends JPanel{
    private int sizex = 500, sizey = 500;

    private Player p;
    private Player p2; // temporary

    private int playerSize = 10;
    private int faceLineSize = 10;

    public GamePanel() {
        setSize(sizex, sizey);
        setBackground(Color.BLACK);
        p = new Player("default", sizex / 2, sizey / 2, 0.0f);
        p2 = new Player("musuh", 10, 10, 0.0f);

        Timer t = new Timer(42, new MyTimerListener());
        t.isRepeats();
        t.start();

        setFocusable(true);
        setFocusTraversalKeysEnabled(false);

        addKeyListener(new MyListener());
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        // Paint player position
        int x = p.getX(), y = p.getY();
        g.setColor(Color.WHITE);
        g.drawOval(x - playerSize / 2, y - playerSize / 2, playerSize, playerSize);

        // Paint Player direction
        int x2 = x + (int) (faceLineSize * Math.cos(p.getFacingAngle()));
        int y2 = y + (int) (faceLineSize * Math.sin(p.getFacingAngle()));
        g.drawLine(x, y, x2, y2);

        // Paint enemy position
        int xp = p2.getX(), yp = p2.getY();
        g.setColor(Color.RED);
        g.drawOval(xp - playerSize / 2, yp - playerSize / 2, playerSize, playerSize);

        // Paint enemy direction
        int x2p = xp + (int) (faceLineSize * Math.cos(p2.getFacingAngle()));
        int y2p = yp + (int) (faceLineSize * Math.sin(p2.getFacingAngle()));
        g.drawLine(xp, yp, x2p, y2p);
    }

    static class MyListener implements KeyListener {
        private static final Set<Integer> pressed = new HashSet<Integer>();

        @Override
        public void keyTyped(KeyEvent e) {

        }

        @Override
        public void keyPressed(KeyEvent e) {
            pressed.add(e.getKeyCode());
            System.out.println("pressed");
            //repaint();
        }

        @Override
        public void keyReleased(KeyEvent e) {
            System.out.println("released");
            pressed.remove(e.getKeyCode());
        }
    }

    class MyTimerListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            // update p1
            if (MyListener.pressed.contains(KeyEvent.VK_W)) p.walk();
            if (MyListener.pressed.contains(KeyEvent.VK_RIGHT)) p.spin(true);
            else if (MyListener.pressed.contains(KeyEvent.VK_LEFT)) p.spin(false);

            // Rotasi p2 ke arah p1
            int dx = (p.getX() - p2.getX()) ;
            int dy = (p.getY() - p2.getY()) ;
            float dist = (float) Math.pow((dx*dx+dy*dy),0.5);
            dx = (int) (4 * dx/dist);
            dy = (int) (4 * dy/dist);

            // Berjalan p2 ke arah p1
            p2.setLoc(dx, dy);

            // repaint
            repaint();
        }
    }
}

