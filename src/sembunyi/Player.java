package sembunyi;

public class Player {
    private String name;       // Nama player
    private float x, y;        // Koordinat player
    private int intx, inty;    // Koordinat player dalam format integer
    private float facingAngle; // Arah pandangan player terhadap utara

    private int energy = 100;  // Energi yang dimiliki player, berkisar 0 sampai 100
    private float life = 100;  // Nyawa player, berkisar 0 sampai 100

    private final int walkSpeed = 5;
    private final int runSpeedIncrement = 20;
    private final float spinRate = (float) (20.0/180 * Math.PI);


    public Player(String name, float x, float y, float facingAngle) {
        this.name = name;
        this.x = x; this.y = y;
        this.facingAngle = facingAngle;

        intx = (int) x; inty = (int) y;
    }

    public void walk() {
        x += Math.cos(facingAngle) * walkSpeed;
        y += Math.sin(facingAngle) * walkSpeed;


        intx = (int) x; inty = (int) y;
    }

    public void setLoc(int x, int y) {
        this.x += x; this.y += y;
        intx += x; inty += y;
    }

    public void spin(boolean clockwise) {
        if (clockwise) facingAngle += spinRate;
        else facingAngle -= spinRate;
    }

    public int getX() { return intx; }

    public int getY() { return inty; }

    public float getFacingAngle() { return facingAngle; }
}
