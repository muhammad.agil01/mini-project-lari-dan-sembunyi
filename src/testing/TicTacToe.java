package testing;

import javax.swing.*;
import java.awt.*;

public class TicTacToe extends JPanel {
    int lineX = 150, lineY = 150;

    public TicTacToe() {
        this.setSize(lineX*3, lineY*3);
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        g.setColor(Color.gray);
        g.drawLine(lineX, 0, lineX, 3*lineY);
        g.drawLine(2*lineX, 0, 2*lineX, 3*lineY);
        g.drawLine(0, lineY, 3*lineX, lineY);
        g.drawLine(0, 2*lineY, 3*lineX, 2*lineY);
    }
}
