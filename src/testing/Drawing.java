package testing;

import javax.swing.*;
import java.awt.*;

public class Drawing extends JPanel {
    private int width = 640, height = 640;
    private int wx = 80, wy = 80;

    private int xrec = 0, yrec = 0;
    private boolean black = true;




    public Drawing() {}

    public void setCoordinate(int x, int y) {
        xrec = x; yrec = y;
    }

    public void calculatePosition(int mouseX, int mouseY) {
        if (mouseX > width) mouseX = width - wx;
        if (mouseY > height) mouseY = height - wy;


        xrec = mouseX - (mouseX % wx);
        yrec = mouseY - (mouseY % wy);
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        //g.setColor(Color.BLACK);
        //g.fillRect(xrec, yrec, xrec2, yrec2);
        for (int x = 0; x < width; x += wx) {
            if (black) black = false;
            else black = true;

            for (int y = 0; y < height ; y += wy) {
                if (black) {
                    g.setColor(Color.BLACK);
                    g.fillRect(x, y, wx, wy);
                    black = false;
                } else {
                    g.setColor(Color.WHITE);
                    g.fillRect(x, y, wx, wy);
                    black = true;
                }
            }

            g.setColor(Color.RED);
            g.fillOval(xrec, yrec, wx, wy);
        }

    }
}
