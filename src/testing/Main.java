package testing;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class Main {
    public static void main(String[] args) {
        run1();
    }

    public static void run1() {
        JFrame f = new JFrame("Drawing GUI");
        Drawing p = new Drawing();

        f.setSize(640, 640);
        f.setVisible(true);
        f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        f.add(p);

        p.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                // Get coordinate floored
                System.out.println("tertekan");
                p.calculatePosition(e.getX(), e.getY());
                p.repaint();
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });
        p.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
                System.out.println(1);
            }

            @Override
            public void keyPressed(KeyEvent e) {
                System.out.println(2);
            }

            @Override
            public void keyReleased(KeyEvent e) {
                System.out.println(3);
            }
        });
    }

    public static void run2() {
        JFrame f = new JFrame("Drawing GUI");
        TicTacToe p = new TicTacToe();

        f.setSize(450, 450);
        f.setLayout(new BorderLayout());
        f.setVisible(true);
        f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        f.add(p);
    }
}
